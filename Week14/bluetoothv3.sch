EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:Radio_Microchip_RN4871 U1
U 1 1 60BF2F2F
P 5550 3200
F 0 "U1" H 5550 3981 50  0000 C CNN
F 1 "Radio_Microchip_RN4871" H 5550 3890 50  0000 C CNN
F 2 "fab:Microchip_RN4871" H 5550 2500 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/50002489A.pdf" H 5050 3750 50  0001 C CNN
	1    5550 3200
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_FTDI_01x06_Male J1
U 1 1 60BF3A60
P 5550 4700
F 0 "J1" V 5634 4342 50  0000 R CNN
F 1 "Conn_FTDI_01x06_Male" V 5543 4342 50  0000 R CNN
F 2 "fab:Header_SMD_FTDI_01x06_P2.54mm_Horizontal_Male" H 5550 4700 50  0001 C CNN
F 3 "~" H 5550 4700 50  0001 C CNN
	1    5550 4700
	0    -1   -1   0   
$EndComp
$Comp
L fab:R R1
U 1 1 60BF53EA
P 7200 4000
F 0 "R1" H 7270 4046 50  0000 L CNN
F 1 "R" H 7270 3955 50  0000 L CNN
F 2 "fab:R_1206" V 7130 4000 50  0001 C CNN
F 3 "~" H 7200 4000 50  0001 C CNN
	1    7200 4000
	1    0    0    -1  
$EndComp
$Comp
L fab:C C1
U 1 1 60BF591F
P 7650 4000
F 0 "C1" H 7765 4046 50  0000 L CNN
F 1 "C" H 7765 3955 50  0000 L CNN
F 2 "fab:C_1206" H 7688 3850 50  0001 C CNN
F 3 "" H 7650 4000 50  0001 C CNN
	1    7650 4000
	1    0    0    -1  
$EndComp
Text Label 6250 3000 0    50   ~ 0
P1_2
Text Label 9050 4200 0    50   ~ 0
VCC
Text Label 8750 4500 0    50   ~ 0
GND
Text Label 5550 2600 0    50   ~ 0
VCC
Text Label 7650 3850 0    50   ~ 0
VCC
Text Label 7650 4150 0    50   ~ 0
GND
Text Label 7200 4150 0    50   ~ 0
VCC
Text Label 7200 3850 0    50   ~ 0
RST
Text Label 5450 3800 3    50   ~ 0
GND
Text Label 5650 3800 3    50   ~ 0
GND
NoConn ~ 6250 3100
NoConn ~ 6250 3200
NoConn ~ 6250 3300
NoConn ~ 6250 3400
NoConn ~ 6250 3500
NoConn ~ 4850 3500
NoConn ~ 4850 3400
NoConn ~ 4850 3100
NoConn ~ 6250 2900
Text Label 4850 3300 2    50   ~ 0
RST
Text Label 4850 2900 2    50   ~ 0
TX
Text Label 4850 3000 2    50   ~ 0
RX
Text Label 5650 4500 0    50   ~ 0
TX
Text Label 5750 4500 0    50   ~ 0
RX
NoConn ~ 5850 4500
Text Label 5350 4500 1    50   ~ 0
GND
NoConn ~ 5450 4500
$Comp
L fab:BUTTON_B3SN SW1
U 1 1 60BFB039
P 7400 3150
F 0 "SW1" H 7400 3435 50  0000 C CNN
F 1 "BUTTON_B3SN" H 7400 3344 50  0000 C CNN
F 2 "fab:Button_Omron_B3SN_6x6mm" H 7400 3350 50  0001 C CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3sn.pdf" H 7400 3350 50  0001 C CNN
	1    7400 3150
	1    0    0    -1  
$EndComp
Text Label 7200 3150 2    50   ~ 0
P1_2
Text Label 7600 3150 0    50   ~ 0
GND
$Comp
L fab:LED D1
U 1 1 60BFBA26
P 7600 4750
F 0 "D1" H 7593 4966 50  0000 C CNN
F 1 "LED" H 7593 4875 50  0000 C CNN
F 2 "fab:LED_1206" H 7600 4750 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 7600 4750 50  0001 C CNN
	1    7600 4750
	1    0    0    -1  
$EndComp
$Comp
L fab:R R2
U 1 1 60BFC19D
P 8150 4750
F 0 "R2" V 7943 4750 50  0000 C CNN
F 1 "R" V 8034 4750 50  0000 C CNN
F 2 "fab:R_1206" V 8080 4750 50  0001 C CNN
F 3 "~" H 8150 4750 50  0001 C CNN
	1    8150 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	8000 4750 7750 4750
Text Label 7450 4750 1    50   ~ 0
GND
$Comp
L Power:PWR_FLAG #FLG0101
U 1 1 60BFEA9C
P 5550 4500
F 0 "#FLG0101" H 5550 4575 50  0001 C CNN
F 1 "PWR_FLAG" H 5550 4673 50  0000 C CNN
F 2 "" H 5550 4500 50  0001 C CNN
F 3 "~" H 5550 4500 50  0001 C CNN
	1    5550 4500
	1    0    0    -1  
$EndComp
Text Label 5550 4500 2    50   ~ 0
VCCI
Text Label 8450 4200 1    50   ~ 0
VCCI
$Comp
L fab:Regulator_Linear_LM3480-3.3V-100mA U2
U 1 1 60BF80C5
P 8750 4200
F 0 "U2" H 8750 4442 50  0000 C CNN
F 1 "Regulator_Linear_LM3480-3.3V-100mA" H 8750 4351 50  0000 C CNN
F 2 "fab:SOT-23" H 8750 4425 50  0001 C CIN
F 3 "https://www.ti.com/lit/ds/symlink/lm3480.pdf" H 8750 4200 50  0001 C CNN
	1    8750 4200
	1    0    0    -1  
$EndComp
Text Label 8300 4750 0    50   ~ 0
VCCI
$EndSCHEMATC
